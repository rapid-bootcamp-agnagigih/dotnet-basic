﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq
{
    internal class IntroLinq
    {
        public IntroLinq() { }

        public static void Introduction()
        {
            // specify the data source.
            int[] score = { 97, 92, 81, 60, 63, 83, 66, 88, 85 };
            // tanpa linq
            // for untuk filter
            List<int> listScore = new List<int>();
            for(int i =0; i < score.Length; i++) 
            {
                if (score[i] > 80)
                {
                    listScore.Add(score[i]);
                }
            }
            // execute the query.
            foreach (int i in listScore)
            {
                Console.WriteLine(i + " ");
            }

        }

        public static void IntroductionWithLinq()
        {
            // specify the data source.
            int[] scores = { 97, 92, 81, 60, 63, 83, 66, 88, 85 };
            // define the query expression.
            IEnumerable<int> listScore =
                from score in scores
                where score > 80
                select score;

            // execute the query
            foreach (int i in listScore)
            {
                Console.WriteLine(i + " ");
            }
        }
    }
}
