﻿using Linq.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq
{
    public class ProgramMain
    {
        public static void Main()
        {
            //IntroductionLinq();
            //Product.SampleFilterProduct();
            Product.SampleFilterByName();
        }

        #region introduction
        public static void IntroductionLinq()
        {
            Console.WriteLine("Introduction withou LinQ : ");
            IntroLinq.Introduction();
            Console.WriteLine("\nIntro with Linq : ");
            IntroLinq.IntroductionWithLinq();
        }
        #endregion

    }
}
