﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq.Model
{
    public class Category
    {
        public String Categories { get; set; }
        public int Count { get; set; }
        public Category(String name, int count)
        {
            Categories = name;
            Count = count;
        }

        // toString
        public override string ToString()
        {
            return "Category { Name : " + this.Categories + ", Price : Rp" + this.Count + " }";
        }


    }
}
