﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq.Model
{
    public class Product
    {
        public string Name { get; set; }
        public int Price { get; set; }

        public Product(string name, int price)
        {
            Name = name;
            Price = price;
        }

        // toString
        public override string ToString()
        {
            return "Product { Name : " + this.Name + ", Price : Rp" + this.Price + " }";
        }

        public static List<Product> GetData()
        {
            List<Product> products = new List<Product>()
            {
                new Product("Mie Goreng", 10000),
                new Product("Mie Ayam", 8000),
                new Product("Bakso", 18000),
                new Product("Jus Jeruk", 8000),
                new Product("Jus Sirsak", 8000),
            };
            return products;
        }

        public static void SampleFilterProduct()
        {
            // create data source
            List<Product> products = GetData();

            // create query
            IEnumerable<Product> productFilter = from item in products
                                                 where item.Price >= 15000
                                                 select item;

            // execute qurey
            foreach (var product in productFilter)
            {
                Console.WriteLine(product);
            }
        }

        public static void SampleFilterByName()
        {
            // create data source
            List<Product> products = GetData();

            // create query
            IEnumerable<Product> productFilter = from product in products
                                                 where product.Name.ToLower().Contains("jus")
                                                 select product;

            // execute qurey
            foreach (var product in productFilter)
            {
                Console.WriteLine(product);
            }
        }
    }
}
