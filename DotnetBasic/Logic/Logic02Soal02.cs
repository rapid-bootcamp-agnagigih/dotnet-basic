﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicDasar
{
    public class Logic02Soal02
    {
        public Logic02Soal02()
        {
        }

        public static void CetakData(int n)
        {
            for (int i = 0; i < n; i++)
            {
                int number = 1;
                for (int j = 0; j < n; j++)
                {
                    if (i == 0 || i == n - 1 || j == 0 || j == n - 1)
                    {
                        Console.Write(number + "\t");
                    }
                    else
                    {
                        Console.Write("\t");
                    }
                    number += 2;
                }
                Console.WriteLine("\n");
            }
        }
    }
}