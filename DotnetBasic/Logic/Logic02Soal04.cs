﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicDasar
{
    public class Logic02Soal04
    {
        public Logic02Soal04() { }
        public static void CetakData(int n)
        {
            int[] deret = new int[n];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if(j <= 1)
                    {
                        deret[j] = 1;
                    }
                    else
                    {
                        deret[j] = deret[j-1] + deret[j-2];
                    }

                    if (n%2 == 0)
                    {
                        if (i == 0 || i == (n / 2) || i == (n / 2) - 1 || i == n - 1 || j == 0
                            || j == (n / 2) || j == (n / 2) - 1 || j == n - 1)
                        {
                            Console.Write(deret[j] + "\t");
                        }
                        else
                        {
                            Console.Write("\t");
                        }
                    }
                    else
                    {
                        if(i == 0 || i == (n / 2) || i == n - 1 || j == 0 || j == (n / 2) || j == n - 1)
                        {
                            Console.Write(deret[j] + "\t");
                        }
                        else
                        {
                            Console.Write("\t");
                        }
                    }

                }
                Console.WriteLine("\n");
            }
        }
    }
}