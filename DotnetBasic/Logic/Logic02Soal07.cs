﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicDasar
{
    public class Logic02Soal07
    {
        public Logic02Soal07() { }

        public static void CetakData(int n)
        {
            int[] deret = new int[n];
            for (int i = 0; i < n; i++)
            {
                if (i <= 1) { deret[i] = 1; }
                else if (i < n / 2 + 1) { deret[i] = deret[i - 1] + deret[i - 2]; }
                else { deret[i] = deret[n - i - 1]; }
            }
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if ((j <= i && j >= (n - i - 1) && j <= n / 2 || (j >= i && j <= (n - i - 1) && j >= n / 2)))
                    {
                        Console.Write(deret[i] + "\t");
                    }
                    else if ((j <= i && j <= (n - i - 1) && i <= n / 2 || (j >= i && j >= (n - i - 1) && i >= n / 2)))
                    {
                        Console.Write(deret[j] + "\t");
                    }
                    else { Console.Write("\t"); }
                }
                Console.WriteLine("\n");
            }
        }
    }
}
