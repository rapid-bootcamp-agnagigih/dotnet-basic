﻿using LogicDasar;

public class MainLogic
{
    public static void Main()
    {
        Console.Write("Masukan Angka : ");
        string? input = Console.ReadLine();
        int n = input == null ? 0 : int.Parse(input);

        Console.WriteLine("Logic 2 Soal 1");
        Logic02Soal01.CetakData(n);
        Console.WriteLine("Logic 2 Soal 2");
        Logic02Soal02.CetakData(n);
        Console.WriteLine("Logic 2 Soal 3");
        Logic02Soal03.CetakData(n);
        Console.WriteLine("Logic 2 Soal 4");
        Logic02Soal04.CetakData(n);
        Console.WriteLine("Logic 2 Soal 5");
        Logic02Soal05.CetakData(n);
        Console.WriteLine("Logic 2 Soal 6");
        Logic02Soal06.CetakData(n);
        Console.WriteLine("Logic 2 Soal 7");
        Logic02Soal07.CetakData(n);
        Console.WriteLine("Logic 2 Soal 8");
        Logic02Soal08.CetakData(n);
        Console.WriteLine("Logic 2 Soal 9");
        Logic02Soal09.CetakData(n);
        Console.WriteLine("Logic 2 Soal 10");
        Logic02Soal10.CetakData(n);
    }
}