﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample101Linq.DataSource
{
    public class AggregateOperator
    {
        public List<Product> GetProductList() => Products.ProductList;
        public List<Customer> GetCustomers() => Customers.CustomerList;

        #region count-syntax
        public int CountSyntax()
        {
            
            int[] factorsOf300 = { 2, 2, 3, 5, 5 };
            int uniqueFactors = factorsOf300.Distinct().Count();

            Console.WriteLine($"There are {uniqueFactors} unique factors of 300.");
            return 0;
            
        }
        #endregion

        #region count conditional
        public int CountConditional()
        {
            int[] numbers = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0 };

            int oddNumbers = numbers.Count(n => n % 2 == 1);
            Console.WriteLine("There are {0} odd numbers in the list");
            return 0;
        }
        #endregion

        #region nested-count
        public int CountNested()
        {
            List<Customer> customers = Customers.GetCustomerList();

            /*var orderCounts = from c in customers
                              select (c.CustomerID, c.CompanyName, OrderCount: c.Orders.Count());
            foreach (var customer in orderCounts)
            {
                Console.WriteLine($"ID: {customer.CustomerID}, Name: {customer.CompanyName}, count: {customer.OrderCount}");
            }*/

            var orderCounts = from c in customers
                              select new
                              {
                                  ID = c.CustomerID,
                                  Name = c.CompanyName,
                                  Count = c.Orders.Count()
                              };

            foreach(var customer in orderCounts)
            {
                Console.WriteLine($"ID: {customer.ID}, Name: {customer.Name}, count: {customer.Count}");
            }

            return 0;
        }
        #endregion

        #region grouped count
        public int GroupedCount()
        {
            List<Product> products = GetProductList();

            var categoryCounts = from p in products
                                 group p by p.Category into g
                                 select (Category: g.Key, ProductCount: g.Count());
            foreach (var c in categoryCounts)
            {
                Console.WriteLine($"Category: {c.Category}: Product count: {c.ProductCount}");
            }
            return 0;
        }
        #endregion

        #region sum of projection
        public int SumProjection()
        {
            string[] words = { "cherry", "apple", "blueberry" };

            double totalChars = words.Sum(w => w.Length);
            Console.WriteLine($"There are a total of {totalChars} characters in these words.");
            return 0;
        }
        #endregion

        #region grouped sum
        public int SumGrouped()
        {
            List<Product> products = GetProductList();

            var categories = from p in products
                             group p by p.Category into g
                             select (Category: g.Key, TotalUnitsInStock: g.Sum(p => p.UnitsInStock));

            foreach (var pair in categories)
            {
                Console.WriteLine($"Category: {pair.Category}, Units in stock: {pair.TotalUnitsInStock}");
            }
            return 0;
        }
        #endregion

        #region min syntax
        public int MinSyntax()
        {
            int[] numbers = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0 };
            int minNum = numbers.Min();

            Console.WriteLine($"The minimum number is {minNum}");
            return 0;
        }
        #endregion

        #region min projection
        public int MinProjection()
        {
            string[] words = { "cherry", "apple", "blueberry" };

            int shortestWord = words.Min(w => w.Length);
            Console.WriteLine($"The shortest word is {shortestWord} characters long.");
            return 0;
        }
        #endregion

        #region min grouped
        public int MinGrouped()
        {
            List<Product> products = GetProductList();
            var categories = from p in products
                             group p by p.Category into g
                             select (Category: g.Key, CheapestPrice: g.Min(p => p.UnitPrice));

            foreach (var c in categories)
            {
                Console.WriteLine($"Category: {c.Category}, Lowest price: {c.CheapestPrice}");
            }
            return 0;
        }
        #endregion

        #region min each group
        public int MinEachGroup()
        {
            List<Product> products = GetProductList();
            var categories = from p in products
                             group p by p.Category into g
                             let minPrice = g.Min(p => p.UnitPrice)
                             select (Category: g.Key, CheapestProducts: g.Where(p => p.UnitPrice == minPrice));

            foreach (var c in categories)
            {
                Console.WriteLine($"Category: {c.Category}");
                foreach (var p in c.CheapestProducts)
                {
                    Console.WriteLine($"\tProduct: {p}");
                }
            }
            return 0;
        }
        #endregion

        #region max syntax
        public int MaxSyntax()
        {
            int[] numbers = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0 };

            int maxNum = numbers.Max();
            return 0;
        }
        #endregion

        #region max projection
        public int MaxProjection()
        {
            string[] words = { "cherry", "apple", "blueberry" };

            int longestLength = words.Max(w => w.Length);
            Console.WriteLine($"The longest word is {longestLength} characters long.");
            return 0;
        }
        #endregion

        #region max grouped
        public int MaxGrouped()
        {
            List<Product> products = GetProductList();
            var categories = from p in products
                             group p by p.Category into g
                             select (Category: g.Key, MostExpensivePrice: g.Max(p => p.UnitPrice));

            foreach (var c in categories)
            {
                Console.WriteLine($"Category: {c.Category} Most expensive product: {c.MostExpensivePrice}");
            }
            return 0;
        }
        #endregion

        #region max each group
        public int MaaxEachGroup()
        {
            List<Product> products = GetProductList();
            var categories = from p in products
                             group p by p.Category into g
                             let maxPrice = g.Max(p => p.UnitPrice)
                             select (Category: g.Key, MostExpensiveProducts: g.Where(p => p.UnitPrice == maxPrice));

            foreach (var c in categories)
            {
                Console.WriteLine($"Category: {c.Category}");
                foreach (var p in c.MostExpensiveProducts)
                {
                    Console.WriteLine($"\t{p}");
                }
            }
            return 0;
        }
        #endregion

        #region average syntax
        public int AverageSyntax()
        {
            int[] numbers = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0 };

            double averageNum = numbers.Average();
            Console.WriteLine($"The average number is {averageNum}.");
            return 0;
        }
        #endregion

        #region average projection
        public int AverageProjection()
        {
            string[] words = { "cherry", "apple", "blueberry" };

            double averageLength = words.Average(w => w.Length);

            Console.WriteLine($"The average word length is {averageLength} characters.");

            return 0;
        }
        #endregion

        #region average grouped
        public int AverageGrouped()
        {
            
            List<Product> products = GetProductList();

            var categories = from p in products
                             group p by p.Category into g
                             select (Category: g.Key, AveragePrice: g.Average(p => p.UnitPrice));

            foreach (var c in categories)
            {
                Console.WriteLine($"Category: {c.Category}, Average price: {c.AveragePrice}");
            }
            
            return 0;
        }
        #endregion

        #region aggregate syntax
        public int AggregateSyntax()
        {
            double[] doubles = { 1.7, 2.3, 1.9, 4.1, 2.9 };
            double product = doubles.Aggregate((runningProduct, nextFactior) => runningProduct * nextFactior);
            Console.WriteLine($"Total product of all numbers: {product}");
            return 0;
        }
        #endregion

        #region seeded aggregate
        public int SeededAggregate()
        {
            double startBalance = 100.0;
            int[] attemptedWithdrawals = { 20, 10, 40, 50, 10, 70, 30 };

            double endBalance = attemptedWithdrawals.Aggregate(startBalance, (balance, nextWithdrawal) => ((nextWithdrawal <= balance) ? (balance - nextWithdrawal) : balance));
            Console.WriteLine($"Ending balance: {endBalance}");
            return 0;
        }
        #endregion

    }
}
