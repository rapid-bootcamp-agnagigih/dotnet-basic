﻿using Sample101Linq.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample101Linq
{
    public class Program
    {
        public static void Main()
        {
            AggregateOperator aggregateOperator = new AggregateOperator();
            //aggregateOperator.CountSyntax();
            Console.WriteLine("Nested Count");
            //aggregateOperator.CountNested();
            aggregateOperator.AverageSyntax();
            aggregateOperator.AverageProjection();
        }
    }
}
