﻿public class Declaration
{
    public static void SampleDeclarative()
    {
        Console.WriteLine("Declarative Test");
        // variable declaration statements
        double area = 0.0;
        Console.WriteLine("Area Value : " + area);
        double radius = 2;
        Console.WriteLine("Radius Value : " + radius);
        // constant declaration statement
        const double pi = 3.13159;
        Console.WriteLine("Pi Value : " + pi);

    }
}