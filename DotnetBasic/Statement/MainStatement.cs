﻿using System;
namespace Statement
{
    public class MainStatement
    {
        public MainStatement()
        {
        }

        public static void Main()
        {
            Declaration.SampleDeclarative();
            ForStatement.SampleFor();
        }
    }
}

